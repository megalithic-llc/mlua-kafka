# mlua-kafka

A Rust-native implementation of [lua-kafka](https://luarocks.org/modules/chensoft/kafka)
for [mlua](https://crates.io/crates/mlua).

[![License](http://img.shields.io/badge/Licence-MIT-blue.svg)](LICENSE)
[![Arch](https://img.shields.io/badge/Arch-aarch64%20|%20amd64-blue.svg)]()
[![Lua](https://img.shields.io/badge/Lua-5.1%20|%205.2%20|%205.3%20|%205.4%20|%20LuaJIT%20|%20LuaJIT%205.2-blue.svg)]()

## Installing

Add to your Rust project using one of MLua's features: [lua51, lua52, lua53, lua54, luajit, luajit52].

```shell
$ cargo add mlua-kafka --features luajit
```

## Using

```rust
use mlua::Lua;

let lua = Lua::new();
mlua_kafka::preload(&lua);
let script = r#"
    local settings = {
        ['bootstrap.servers'] = '127.0.0.1:9092',
    }
    local producer = require('kafka').producer(settings)
    producer:produce('topic', 'mykey', 'myvalue')
    producer:flush(100)
"#;
lua.load(script).exec()?;
```
 
## Integration Testing

```shell
$ KAFKA_BROKERS=127.0.0.1:9092 KAFKA_TOPIC=mytopic cargo test
```
