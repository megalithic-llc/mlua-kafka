use super::Consumer;
use mlua::{Error, Lua, MultiValue, Value};
use rdkafka::consumer::Consumer as RdKafkaConsumer;

pub(super) fn handle(_lua: &Lua, consumer: &Consumer, topics: MultiValue) -> Result<(), Error> {
    let args: Vec<Value> = topics.into_iter().collect();
    let topics: Vec<String> = args.iter().map(|x| x.to_string().unwrap()).collect();
    let topics: Vec<_> = topics.iter().map(String::as_str).collect();
    consumer
        .consumer
        .subscribe(&topics)
        .map_err(|err| Error::RuntimeError(err.to_string()))?;
    Ok(())
}
