use super::Producer;
use mlua::{Error, Lua, Value};

pub(super) fn handle(_lua: &Lua, _producer: &Producer, _arg: Value) -> Result<(), Error> {
    Ok(())
}
