mod close;
mod flush;
mod poll;
mod produce;

use mlua::{UserData, UserDataMethods};
use rdkafka::producer::BaseProducer;
use std::sync::Arc;

#[derive(Clone)]
pub(super) struct Producer {
    pub producer: Arc<BaseProducer>,
}

impl UserData for Producer {
    fn add_methods<M: UserDataMethods<Self>>(methods: &mut M) {
        methods.add_method("close", close::handle);
        methods.add_method("flush", flush::handle);
        methods.add_method("poll", poll::handle);
        methods.add_method("produce", produce::handle);
    }
}
