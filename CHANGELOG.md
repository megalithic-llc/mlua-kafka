# mlua-kafka Changelog

## [0.1.7] - 2024-10-29
### Changed
- [#16](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/16) Upgrade from mlua 0.9.9 → 0.10.0

## [0.1.6] - 2024-10-19
### Changed
- [#15](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/15) Upgrade Rust from 1.79.0 → 1.82.0

## [0.1.5] - 2024-07-28
### Changed
- [#14](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/14) Upgrade Rust from 1.72.1 → 1.79.0
- [#13](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/13) Upgrade 3 crates
- [#12](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/12) Preload should return a mlua RuntimeError for easier integration

## [0.1.4] - 2024-01-13
### Changed
- [#11](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/11) Upgrade 2 crates

## [0.1.3] - 2023-12-17
### Changed
- [#9](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/9) Downgrade rdkafka from 0.36.0 → 0.35.0 after 0.36.0 was yanked

## [0.1.2] - 2023-12-03
### Added
- [#8](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/8) Add badges to README for architecture and Lua VM support
- [#6](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/6) Provide an 'outdated' make target

### Changed
- [#7](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/7) Upgrade 2 crates
- [#5](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/5) Use build matrix to simplify GitLab CI config

## [0.1.1] - 2023-11-04
### Added
- [#4](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/4) Provide a README

### Changed
- [#3](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/3) Test merge requests with a matrix of Lua VM versions
- [#2](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/2) Publish tag releases to crates.io

## [0.1.0] - 2023-10-07
### Added
- [#1](https://gitlab.com/megalithic-llc/mlua-kafka/-/issues/1) Initial support for producer and consumer
