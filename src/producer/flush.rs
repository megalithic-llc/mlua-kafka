use super::Producer as ProducerUserData;
use mlua::{Error, Lua};
use rdkafka::producer::Producer;
use std::time::Duration;

pub(super) fn handle(_lua: &Lua, producer: &ProducerUserData, timeout_ms: u64) -> Result<(), Error> {
    let dur = Duration::from_millis(timeout_ms);
    producer
        .producer
        .flush(dur)
        .map_err(|err| Error::RuntimeError(err.to_string()))?;
    Ok(())
}
