use super::Producer;
use mlua::{Error, Lua};
use std::time::Duration;

pub(super) fn handle(_lua: &Lua, producer: &Producer, timeout_ms: u64) -> Result<(), Error> {
    let dur = Duration::from_millis(timeout_ms);
    producer.producer.poll(dur);
    Ok(())
}
