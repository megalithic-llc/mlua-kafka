use super::Producer;
use mlua::{Error, Lua, MultiValue};
use rdkafka::producer::BaseRecord;

pub(super) fn handle(_lua: &Lua, producer: &Producer, args: MultiValue) -> Result<(), Error> {
    let topic = args[0].to_string()?;
    let k: String = args[1].to_string()?;
    let v: String = args[2].to_string()?;
    producer
        .producer
        .send(BaseRecord::to(&topic).key(&k).payload(&v))
        .map_err(|(err, _)| Error::RuntimeError(err.to_string()))?;
    Ok(())
}
